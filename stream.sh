#!/bin/bash

if test -f ".env"; then
    source .env
fi

VBR="2500k"                                    # Bitrate
FPS="15"
QUAL="faster"                                  # Quality preset
YOUTUBE_URL="rtmp://a.rtmp.youtube.com/live2"

SOURCE="/dev/video0"

ffmpeg \
    -f lavfi -i anullsrc -c:a aac \
    -f video4linux2 -video_size 640x480 -i "$SOURCE" -preset $QUAL -c:v libx264 -pix_fmt yuv422p -threads 4 -r $FPS -b:v $VBR \
    -f flv "$YOUTUBE_URL/$YOUTUBE_STREAM_KEY"




