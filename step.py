import RPi.GPIO as GPIO
import time
import logging

PRE_FEED_LED = 33
DRIVER_ON_OFF = 35
MOTOR_STEP = 37

MICROSTEPS_PER_STEP = 8


def step():
    # Flash with LED to notify fish food is coming
    for i in range(0, 5):
        GPIO.output(PRE_FEED_LED, GPIO.HIGH)
        time.sleep(0.5)
        GPIO.output(PRE_FEED_LED, GPIO.LOW)
        time.sleep(0.5)

    logging.info("Turn on driver")
    GPIO.output(DRIVER_ON_OFF, GPIO.HIGH)
    time.sleep(1)

    steps = 1

    sleepSec = 0.5 / MICROSTEPS_PER_STEP / steps
    logging.info("Doing " + str(steps) + " steps")
    for i in range(0, steps * MICROSTEPS_PER_STEP):
        GPIO.output(MOTOR_STEP, GPIO.HIGH)
        time.sleep(sleepSec)
        GPIO.output(MOTOR_STEP, GPIO.LOW)
        time.sleep(sleepSec)

    time.sleep(1)
    logging.info("Turn off driver")
    GPIO.output(DRIVER_ON_OFF, GPIO.LOW)


if __name__ == "__main__":
    try:
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(DRIVER_ON_OFF, GPIO.OUT)
        GPIO.setup(MOTOR_STEP, GPIO.OUT)
        GPIO.setup(PRE_FEED_LED, GPIO.OUT)

        logging.basicConfig(
            filename="/var/feeder/feed.log",
            level=logging.INFO,
            format="%(asctime)s %(levelname)-8s %(message)s",
            datefmt="%Y-%m-%d %H:%M:%S",
        )

        logging.info("Script starting")
        step()
        logging.info("Script finished")
    except Exception as e:
        logging.error("Fail: " + repr(e))
        print("Failed, bye")
    finally:
        GPIO.cleanup()
