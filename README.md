# Fish feeder with  RaspberryPi

```bash
cd ~
git clone ...

cd /var
sudo mkdir feeder
sudo chown pi feeder

cd ~/fish-feeder/
chmod +x run.sh
cp .env.example .env
# Fill Healthchecks.io slug

crontab -e
# add: 0 * * * * /home/pi/fish-feeder/run.sh >> /var/feeder/feed.log 2>&1

# First run will create /var/feeder/next.run file with next day
./run.sh
```
