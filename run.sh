#!/bin/bash

SCRIPT=`realpath $0` 
SCRIPTPATH=`dirname $SCRIPT`

if test -f "$SCRIPTPATH/.env"; then
    set -a
    source "$SCRIPTPATH/.env"
    set +a
fi

python3 "$SCRIPTPATH/feed.py"
