import RPi.GPIO as GPIO
import time
import datetime
import requests
import logging
import os
from urllib3.util.retry import Retry
from requests.adapters import HTTPAdapter

PRE_FEED_LED = 33
DRIVER_ON_OFF = 35
MOTOR_STEP = 37

# Do LOW steps every 1st and 2nd day
STEPS_PER_FEED_DOSE_LOW = 16
# Do HIGH steps every 3rd day to synchronize feeder back
STEPS_PER_FEED_DOSE_HIGH = 18
MICROSTEPS_PER_STEP = 8


def feed():
    # Flash with LED to notify fish food is coming
    for i in range(0, 15):
        GPIO.output(PRE_FEED_LED, GPIO.HIGH)
        time.sleep(0.5)
        GPIO.output(PRE_FEED_LED, GPIO.LOW)
        time.sleep(0.5)

    logging.info("Turn on driver")
    GPIO.output(DRIVER_ON_OFF, GPIO.HIGH)
    time.sleep(1)

    steps = (
        STEPS_PER_FEED_DOSE_HIGH
        if datetime.datetime.now().day % 3 == 0
        else STEPS_PER_FEED_DOSE_LOW
    )

    sleepSec = 0.5 / MICROSTEPS_PER_STEP / steps
    logging.info("Doing " + str(steps) + " steps")
    for i in range(0, steps * MICROSTEPS_PER_STEP):
        GPIO.output(MOTOR_STEP, GPIO.HIGH)
        time.sleep(sleepSec)
        GPIO.output(MOTOR_STEP, GPIO.LOW)
        time.sleep(sleepSec)

    time.sleep(1)
    logging.info("Turn off driver")
    GPIO.output(DRIVER_ON_OFF, GPIO.LOW)


def doRequest(action="", data=None):
    slug = os.environ.get("HEALTHCHECK_SLUG")
    if slug == "" or slug == None:
        logging.warning("No HealthCheck slug")
        return

    try:
        s = requests.Session()

        retries = Retry(
            total=5, backoff_factor=0.1, status_forcelist=[500, 502, 503, 504]
        )
        s.mount("https://", HTTPAdapter(max_retries=retries))

        if action != "":
            action = "/" + action
        res = s.post(
            "https://hc-ping.com/" + slug + "/feeding" + action,
            data=data,
            headers={"User-Agent": "RPi Fish Feeder"},
        )
        if res.status_code < 200 or res.status_code > 299:
            logging.warning("Failed to execute request: " + str(res.status_code))
    except Exception as e:
        logging.warning("Failed to execute request: " + repr(e))


def run():
    now = datetime.datetime.now()
    tomorrowRun = (
        (now + datetime.timedelta(days=1))
        .replace(hour=8, minute=0, second=0, microsecond=0)
        .strftime("%Y-%m-%d %H:%M:%S")
    )

    with open("/var/feeder/next.run", "a+") as f:
        f.seek(0)
        line = f.readline().strip()
        if line == "":
            logging.info("No next.run file, creating: " + tomorrowRun)
            f.write(tomorrowRun)
            return

        nextRun = datetime.datetime.strptime(line, "%Y-%m-%d %H:%M:%S")
        if nextRun > now:
            logging.info(
                "Still in future, skipping: " + nextRun.strftime("%Y-%m-%d %H:%M:%S")
            )
            return

        logging.info("Writing new time: " + tomorrowRun)
        f.truncate(0)
        f.write(tomorrowRun)

    logging.info("Start feeding")

    doRequest("start")
    feed()
    doRequest()

    logging.info("Feeding completed")


if __name__ == "__main__":
    try:
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(DRIVER_ON_OFF, GPIO.OUT)
        GPIO.setup(MOTOR_STEP, GPIO.OUT)
        GPIO.setup(PRE_FEED_LED, GPIO.OUT)

        logging.basicConfig(
            filename="/var/feeder/feed.log",
            level=logging.INFO,
            format="%(asctime)s %(levelname)-8s %(message)s",
            datefmt="%Y-%m-%d %H:%M:%S",
        )

        logging.info("Script starting")
        run()
        logging.info("Script finished")
    except Exception as e:
        doRequest("fail", repr(e))
        logging.error("Fail: " + repr(e))
        print("Failed, bye")
    finally:
        GPIO.cleanup()
